#!/bin/bash

set -x
set -e

d=$(mktemp -d)
cd $d

# sid

debootstrap --variant=minbase sid sid-root http://localhost:9999/debian
mount --bind /proc sid-root/proc
cp /etc/hosts /etc/resolv.conf sid-root/etc/
chroot sid-root apt-get update
chroot sid-root apt-get -y install debfoster
chroot sid-root apt-get -y install nvi less apt debfoster apt-utils
yes | chroot sid-root debfoster -q -f -n -o UseRecommends=no -o MaxPriority=required nvi less apt debfoster apt-utils
chroot sid-root apt-get clean
echo "
debconf	debconf/frontend	select	Noninteractive
debconf	debconf/priority	select	critical
linux-image-2.6.32-5-amd64	linux-image-2.6.32-5-amd64/prerm/removing-running-kernel-2.6.32-5-amd64	boolean	false
linux-image-3.0.0-1-amd64	linux-image-3.0.0-1-amd64/prerm/removing-running-kernel-3.0.0-1-amd64	boolean	false
" | chroot sid-root debconf-set-selections
cat << EOF > sid-root/root/maintscripts
#!/bin/bash
[ "\$DPKG_HOOK_ACTION" == 'unpack' ] || exit 0
for i in /var/lib/dpkg/info/*.{post,pre}{inst,rm}; do
   if sed 1q \$i | grep -q -E "/bin/(ba)?sh"; then
      if ! sed 2q \$i | grep -q -- -x; then
	echo "Dpkg hook: enabling sh trace for \$i"
        sed -i '2i set -x' \$i
      fi
   fi
done
EOF
chmod 755 sid-root/root/maintscripts
mkdir -p sid-root/etc/dpkg
echo 'post-invoke=/root/maintscripts' > sid-root/etc/dpkg/dpkg.cfg

echo "#!/bin/sh
exit 101
" > sid-root/usr/sbin/policy-rc.d
chmod 755 sid-root/usr/sbin/policy-rc.d
umount sid-root/proc
tar czf sid64-instest.tgz -C sid-root .

# jessie

debootstrap --variant=minbase jessie jessie-root http://localhost:9999/debian
mount --bind /proc jessie-root/proc
cp /etc/hosts /etc/resolv.conf jessie-root/etc/
chroot jessie-root apt-get update
chroot jessie-root apt-get -y install debfoster
chroot jessie-root apt-get -y install nvi less apt debfoster apt-utils
yes | chroot jessie-root debfoster -q -f -n -o UseRecommends=no -o MaxPriority=required nvi less apt debfoster apt-utils
chroot jessie-root apt-get clean
echo "
debconf	debconf/frontend	select	Noninteractive
debconf	debconf/priority	select	critical
linux-image-3.2.0-4-amd64       linux-image-3.2.0-4-amd64/prerm/removing-running-kernel-3.2.0-4-amd64	boolean	false
linux-image-3.16.0-4-amd64      linux-image-3.16.0-4-amd64/prerm/removing-running-kernel-3.16.0-4-amd64	boolean	false
" | chroot jessie-root debconf-set-selections
cat << EOF > jessie-root/root/maintscripts
#!/bin/bash
[ "\$DPKG_HOOK_ACTION" == 'unpack' ] || exit 0
for i in /var/lib/dpkg/info/*.{post,pre}{inst,rm}; do
   if sed 1q \$i | grep -q -E "/bin/(ba)?sh"; then
      if ! sed 2q \$i | grep -q -- -x; then
	echo "Dpkg hook: enabling sh trace for \$i"
        sed -i '2i set -x' \$i
      fi
   fi
done
EOF
chmod 755 jessie-root/root/maintscripts
mkdir -p jessie-root/etc/dpkg
echo 'post-invoke=/root/maintscripts' > jessie-root/etc/dpkg/dpkg.cfg

echo "#!/bin/sh
exit 101
" > jessie-root/usr/sbin/policy-rc.d
chmod 755 jessie-root/usr/sbin/policy-rc.d
umount jessie-root/proc
chmod a+rwt jessie-root/dev/shm
tar czf jessie64-instest.tgz -C jessie-root .


# wheezy
debootstrap --variant=minbase wheezy wheezy-root http://localhost:9999/debian
mount --bind /proc wheezy-root/proc
cp /etc/hosts /etc/resolv.conf wheezy-root/etc/
chroot wheezy-root apt-get update
chroot wheezy-root apt-get -y install debfoster
chroot wheezy-root apt-get -y install nvi less apt debfoster apt-utils
yes | chroot wheezy-root debfoster -q -f -n -o UseRecommends=no -o MaxPriority=required nvi less apt debfoster apt-utils
chroot wheezy-root apt-get clean
echo "
debconf	debconf/frontend	select	Noninteractive
debconf	debconf/priority	select	critical
linux-image-2.6.32-5-amd64	linux-image-2.6.32-5-amd64/prerm/removing-running-kernel-2.6.32-5-amd64	boolean	false
linux-image-3.2.0-4-amd64	linux-image-3.2.0-4-amd64/prerm/removing-running-kernel-3.2.0-4-amd64	boolean	false
" | chroot wheezy-root debconf-set-selections
cat << EOF > wheezy-root/root/maintscripts
#!/bin/bash
[ "\$DPKG_HOOK_ACTION" == 'unpack' ] || exit 0
for i in /var/lib/dpkg/info/*.{post,pre}{inst,rm}; do
   if sed 1q \$i | grep -q -E "/bin/(ba)?sh"; then
      if ! sed 2q \$i | grep -q -- -x; then
	echo "Dpkg hook: enabling sh trace for \$i"
        sed -i '2i set -x' \$i
      fi
   fi
done
EOF
chmod 755 wheezy-root/root/maintscripts
mkdir -p wheezy-root/etc/dpkg
echo 'post-invoke=/root/maintscripts' > wheezy-root/etc/dpkg/dpkg.cfg

echo "#!/bin/sh
exit 101
" > wheezy-root/usr/sbin/policy-rc.d
chmod 755 wheezy-root/usr/sbin/policy-rc.d
umount wheezy-root/proc
rm wheezy-root/dev/shm
mkdir wheezy-root/dev/shm
chmod a+rwt wheezy-root/dev/shm
tar czf wheezy64-instest.tgz -C wheezy-root .


##############

mv sid64-instest.tgz unstable-instest.tgz
mv jessie64-instest.tgz jessie-instest.tgz
mv wheezy64-instest.tgz wheezy-instest.tgz
mv *.tgz /cloud/chroots/
