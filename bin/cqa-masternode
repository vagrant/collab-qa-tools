#!/usr/bin/env ruby
# Author:: Sebastien Badia <seb@sebian.fr>, Lucas Nussbaum <lucas@debian.org>
# Date:: Wed Mar 21 01:18:02 +0100 2012
#

require 'fileutils'
require 'json'
require 'net/ssh'
require 'net/scp'
require 'net/ssh/multi'
require 'peach'
require 'pp'
require 'tempfile'
require 'optparse'

$nodes = nil
$tasks = nil
$slots_per_node=1
$halt_nodes = true
$reprepro = nil
$reprepro_lock = Mutex::new
$reprepro_path = "/root/cloud-scripts/reprepro/"
$logs_dir = "/tmp/logs/"
$retry_failed = true

SLEEPTIME=60
SSH_TIMEOUT=60
MODES_PATH = File.expand_path(File.join(File.dirname(__FILE__), '../modes'))

MASTERNODE_CONFIG = {
  # modes that don't have an executable file
  :pseudo_modes => ["binary-only", "parallel", 'arch-all-only', 'binarch-only']
}

options = OptionParser::new do |opts|
  opts.banner = "Usage: ./masternode [options]"
  opts.separator ""
  opts.separator "Options:"
  opts.on("-t", "--tasks FILE", "Tasks file") do |t|
    $tasks = JSON::parse(IO::read(t))
  end
  opts.on("-n", "--nodes FILE", "Nodes file") do |n|
    $nodes=IO::readlines(n).map { |l| l.chomp }
  end
  opts.on("-s", "--slots SLOTS", "Slots per node") do |n|
    $slots_per_node = n.to_i
  end
  opts.on("", "--no-halt", "Do not halt nodes when no tasks remaining") do
    $halt_nodes = false
  end
  opts.on("", "--no-retry", "Do not retry failed builds") do
    $retry_failed = false
  end
  opts.on("", "--reprepro CODENAME", "Upload built packages to reprepro") do |c|
    $reprepro = c
  end
  opts.on("", "--reprepropath PATH", "Point reprepro to this path") do |p|
    $reprepro_path = p
  end
  opts.on("-d", "--logs-dir LOGSDIR", "Put build logs in LOGSDIR, defaults to /tmp/logs") do |d|
    # force final trailing separator but let's try not to be fascist about
    d += File::SEPARATOR unless d[-1] == File::SEPARATOR
    $logs_dir = d
  end
  opts.on("-o", "--output LOGFILE", "Specify logfile for output") do |n|
    log = File::new(n, "w")
    STDOUT.reopen(log)
    STDERR.reopen(log)
    STDOUT.sync = true
    STDERR.sync = true
  end

end
options.parse!(ARGV)


#############################
$startt = Time::now
MSG_ERROR=0       # Exit return codes
MSG_WARNING=1
MSG_INFO=2

def format_seconds(s)
  d = s / 60
  h = d / 60
  m = d % 60

  return "%.2d:%.2d" % [h, m]
end

$output_mutex = Mutex::new
def msg(str, type=nil, quit=false)
  $output_mutex.synchronize do
    case type
    when MSG_ERROR
      puts("### Error: #{str} ###")
    when MSG_WARNING
      puts("### Warning: #{str} ###")
    when MSG_INFO
      elapsed = format_seconds((Time.now - $startt).to_i)
      puts("[#{elapsed}] #{str}")
    else
      puts str
    end
    STDOUT.flush
  end
  exit 1 if quit
end # def:: msg

def open_channel(session, group = nil)
  if group.is_a?(Symbol)
    session.with(group).open_channel do |channel|
      yield(channel)
    end
  elsif group.is_a?(Array)
    session.on(*group).open_channel do |channel|
      yield(channel)
    end
  elsif group.is_a?(Net::SSH::Multi::Server)
    session.on(group).open_channel do |channel|
      yield(channel)
    end
  else
    session.open_channel do |channel|
      yield(channel)
    end
  end
end # def:: open_channel

def nexec(session, cmd, group = nil, critical = true, showerr = true, showout = true)
  outs = {}
  errs = {}
  channel = open_channel(session,group) do |chtmp|
    chtmp.exec(cmd) do |ch, success|
      unless success
        msg("unable to execute '#{cmd}' on #{ch.connection.host}",MSG_ERROR)
      end
        msg("Executing '#{cmd}' on #{ch.connection.host}]",MSG_INFO) \
        if showout
    end
  end
  channel.on_data do |chtmp,data|
    outs[chtmp.connection.host] = [] unless outs[chtmp.connection.host]
    outs[chtmp.connection.host] << data.strip
    msg("[#{chtmp.connection.host}] #{data.strip}") \
    if showout
  end
  channel.on_extended_data do |chtmp,type,data|
    errs[chtmp.connection.host] = [] unless errs[chtmp.connection.host]
    errs[chtmp.connection.host] << data.strip
    msg("[#{chtmp.connection.host} E] #{data.strip}") \
      if showout
  end

  channel.on_request("exit-status") do |chtmp, data|
    status = data.read_long
    if status != 0
      if showerr or critical
        msg("exec of '#{cmd}' on #{chtmp.connection.host} failed " \
            "with return status #{status.to_s}",MSG_ERROR)
        msg("---stdout dump---")
        outs[chtmp.connection.host].each { |out| msg(out) } if \
          outs[chtmp.connection.host]
        msg("---stderr dump---")
        errs[chtmp.connection.host].each { |err| msg(err) } if \
          errs[chtmp.connection.host]
        msg("---\n")
      end
      exit 1 if critical
    end
  end
  channel.wait
  return outs
end # def:: nexec

#############################

STDOUT.sync = true
STDERR.sync = true

# find needed modes
file_modes = []
$tasks.each do |t|
  t['modes'].each do |m|
    next if MASTERNODE_CONFIG[:pseudo_modes].include?(m)
    next if file_modes.include?(m)

    # chech for existence
    raise "unable to find #{m} mode file" unless File.file?(File.join(MODES_PATH, m))

    file_modes << m
  end
end

msg("Preparing nodes ...", MSG_INFO)

failed_nodes = []
dir = File::expand_path(File::dirname(__FILE__))

$nodes.each do |node|
  puts "scping to #{node} ..."

  begin
    Timeout.timeout(SSH_TIMEOUT) do
      Net::SCP.start(node, 'root') do |scp|
        scp.upload!(dir + "/cqa-process-task","/root/")
        #scp.upload!("instest","/root/")
        #scp.upload!("config.rb","/root/")

        # upload needed modes files
        file_modes.each do |m|
          scp.upload!(File.join(MODES_PATH, m), "/tmp/")
        end
      end
    end
  rescue Exception => e
    puts "node #{node} not responding, removing ... (#{e})"
    failed_nodes << node
  end
end

failed_nodes.each { |f| $nodes.delete(f) }

$tasks.shuffle!
$tasks.sort! { |a, b| (a['esttime'] || 50000) <=> (b['esttime'] || 50000) }
$tasks_mutex = Mutex::new
$retry_tasks = []
$running_tasks = []

FileUtils::mkdir_p($logs_dir)

def execute_one_task(node, slot, ssh, task)
  tname = task['source'] || task['package'] if not task.nil?
  msg("#{node}-#{slot}: executing #{tname}", MSG_INFO)
  u = { :node => node, :slot => slot, :task_name => tname, :since => Time::now, :task => task }
  $tasks_mutex.synchronize do
    $running_tasks << u
  end
  if $reprepro
     task['keep-result'] = `mktemp -u /tmp/result.#{task['source']}.XXXXXX`.chomp
  end
  f = Tempfile.new(tname)
  f.puts JSON::pretty_generate(task)
  f.close
  res = nil
  begin
    ssh.scp.upload!(f.path, "/tmp/")
    ssh.exec!("/root/cqa-process-task -t #{f.path}")
    system("rsync -aqz root@#{node}:#{task['logfile']} #{$logs_dir}/") or raise 'rsync error'
    ssh.exec!("rm -f #{f.path} #{task['logfile']}")
    str = IO::read("#{$logs_dir}#{File::basename(task['logfile'])}")
    # force encoding.
    str.encode!('UTF-16', 'UTF-8', :invalid => :replace, :replace => '')
    str.encode!('UTF-8', 'UTF-16')
    status = str.split(/\n/).grep(/^DC-Status: /)
    if status.empty? or status[0].split[1] != 'OK'
      res = :failed
    else
      res = :ok
      if $reprepro
  #      Thread::new(task) do |task|
  #        Thread::abort_on_exception = true
            d = `mktemp -d /tmp/dl.#{task['source']}.XXXXXX`.chomp
            ssh.scp.download!("#{task['keep-result']}/", d, {:recursive => true })
     	    ssh.exec!("rm -rf #{task['keep-result']}")
          $reprepro_lock.synchronize do
            system("reprepro --ignore=wrongdistribution -b #{$reprepro_path} include #{$reprepro} #{d}/*/*changes") or raise
          end
          system("rm -rf #{d}") or raise
#        end
      end
    end
  rescue Exception => e
    puts e.message
    puts e.backtrace
    res = :exception
  end
  $tasks_mutex.synchronize do
    $running_tasks.delete(u)
  end
  return res
end

$tasks_total = $tasks.length
$tasks_ok = 0
$tasks_retry = 0
$tasks_failed = 0

msg("Starting #{$tasks.length} tasks on #{$nodes.length} nodes (#{$slots_per_node} slots per node) ...", MSG_INFO)
$ths = []
$used_slots = {}
$nodes.each do |node|
  $used_slots[node] = $slots_per_node
  1.upto($slots_per_node) do |slot|
    $ths << Thread::new(node, slot) do |node, slot|
      Thread::abort_on_exception = true
      msg("Starting #{node}-#{slot}", MSG_INFO)
      ssh = Net::SSH.start(node, 'root')
      # first set of tasks
      done = false
      while not done do
        task = nil
        $tasks_mutex.synchronize do
          task = $tasks.pop
        end
        if task.nil?
          msg("#{node}-#{slot}: no tasks remaining, stopping this slot", MSG_INFO)
          done = true
          $tasks_mutex.synchronize do
            $used_slots[node] -= 1
          end
        else
          res = execute_one_task(node, slot, ssh, task)
          tname = task['source'] || task['package'] if not task.nil?
          if res == :failed
            if $retry_failed
               msg("#{node}-#{slot}: #{tname} failed, adding to retry_tasks", MSG_INFO)
               $tasks_mutex.synchronize do
                 $retry_tasks.unshift(task)
               end
               $tasks_retry += 1
           else
               msg("#{node}-#{slot}: #{tname} failed, not retrying", MSG_INFO)
               $tasks_failed += 1
           end
          elsif res == :exception
            if $retry_failed
               msg("#{node}-#{slot}: #{tname} generated an exception, adding to retry_tasks and stopping this slot", MSG_INFO)
               $tasks_mutex.synchronize do
                 $retry_tasks.unshift(task)
               end
               $tasks_retry += 1
            else
               msg("#{node}-#{slot}: #{tname} generated an exception, not retrying", MSG_INFO)
               $tasks_failed += 1
            end
            done = true
            $tasks_mutex.synchronize do
              $used_slots[node] -= 1
            end
          else
            $tasks_ok += 1
          end
        end
      end
      # second set of tasks
      if $used_slots[node] == 0
        msg("#{node}-#{slot}: last slot. Looking at retry tasks.", MSG_INFO)
        done = false
        while not done do
          task = nil
          $tasks_mutex.synchronize do
            task = $retry_tasks.pop
          end
          if task.nil?
            msg("#{node}-#{slot}: no tasks remaining, stopping this node.", MSG_INFO)
            done = true
          else
	    $tasks_retry -= 1
            res = execute_one_task(node, slot, ssh, task)
            tname = task['source'] || task['package'] if not task.nil?
            if res == :failed
	      $tasks_failed += 1
              msg("#{node}: #{tname} failed again", MSG_INFO)
            elsif res == :exception
	      $tasks_failed += 1
              msg("#{node}: #{tname} generated an exception again, stopping this slot", MSG_INFO)
              done = true
            else # :ok
	      $tasks_ok += 1
              msg("#{node}: #{tname} -- retry task succeeded!", MSG_INFO)
            end
          end
        end
        if $halt_nodes
	  msg("#{node}: no slots in use, halting", MSG_INFO)
	  begin
	    ssh.exec!("poweroff &")
	  rescue
            msg("#{node}: HALTING RESULTED IN A EXCEPTION. THE INSTANCE MIGHT STILL BE RUNNING!!", MSG_INFO)
          end
	end
      end
    end
  end
end

$ths << Thread::new do
  Thread::abort_on_exception = true
  while true do
    sleep SLEEPTIME
    tasks = nil
    rtasks = nil
    runtasks = nil
    $tasks_mutex.synchronize do
      tasks = $tasks.dup
      rtasks = $retry_tasks.dup
      runtasks = $running_tasks.dup
    end
    break if tasks.length == 0 and rtasks.length == 0 and runtasks.empty?
    to_process = $tasks_total - $tasks_ok - $tasks_failed - $tasks_retry
    msg("*** total: #{$tasks_total} OK: #{$tasks_ok} (#{$tasks_ok * 100/$tasks_total}%) To process: #{to_process} (#{to_process * 100 / $tasks_total}%) To retry: #{$tasks_retry} (#{$tasks_retry * 100/$tasks_total}%) Failed: #{$tasks_failed} (#{$tasks_failed * 100/$tasks_total}%)", MSG_INFO)
    if tasks.length == 0
      msg("Running tasks:")
      runtasks.sort { |a,b| a[:since] <=> b[:since] }.each do |t|
        msg("#{t[:since]}\t#{t[:node]}\t#{t[:slot]}\t#{t[:task_name]}")
      end
    end
  end
end

# wait all threads
$ths.each do |th|
  th.join
end

puts "Exiting."
